(function(){
	'use strict';

	angular
		.module('app')
		.factory('colourUtilsService', colourUtilsService);

	colourUtilsService.$inject = [];

	function colourUtilsService() {
		var service = {
			mixColourWithTint: mixColourWithTint
		};

		return service;

		function capRgb(rgbEntry){
			var rgbMin = 0,
				rgbMax = 255;

			if(rgbEntry > rgbMax){
				return rgbMax;
			}

			if(rgbEntry < rgbMin){
				return rgbMin;
			}

			return rgbEntry;
		}

		function mixAndCapAll(mixed, tint){
			
			for(var i = 0; i < tint.length; i++){
				mixed[i] = capRgb(mixed[i] + tint[i]);
			}

			return mixed;
		}

		/**
			@param colour{Array}, tint{Array}
		*/
		function mixColourWithTint(colour, tint) {
			var mixed = angular.copy(colour);

			if(tint){
				mixed = mixAndCapAll(mixed, tint);
			}

			return mixed;
		}

		
	}

})();