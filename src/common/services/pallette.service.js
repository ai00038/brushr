(function(){
	'use strict';

	angular
		.module('app')
		.factory('palletteService', palletteService);

	palletteService.$inject = ['$http', 'apiUrl'];

	function palletteService($http, apiUrl) {
		var service = {
			getPallette: getPallette,
			addColourToPallette: addColourToPallette
		};

		return service;

		function getPallette(){
			return $http.get(apiUrl);
		}

		function addColourToPallette(colour){
			var rgbColour = {};
		    rgbColour.name =  "SlipR Newly Added";
		    rgbColour.baseColour = [249, 192, 189];
		    rgbColour.tints = [
		      [-4, -35, -37],
		      [-7, -85, -93],
		      [-20, -128, -152],
		      [-43, -136, -150],
		      [-72, -145, -152]
		    ];
		  
		    return $http.post(apiUrl, rgbColour);
		}
	}

})();