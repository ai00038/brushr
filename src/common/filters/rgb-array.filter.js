(function(){
	'use strict';

	angular
		.module('app')
		.filter('rgbArray', rgbArray);

	function rgbArray() {
	    return filterFunction;
	}

	function filterFunction(rgbArr) {
		return `rgb(${rgbArr[0]}, ${rgbArr[1]}, ${rgbArr[2]})`;
	};


})();