(function(){
	'use strict';

	angular
		.module('app')
		.component('rgbSlider', {
			templateUrl: 'src/common/components/rgb-slider/rgb-slider.component.html',
			bindings: {
				rgbModel: '<',
				isTint: '<',
				onRgbChange: '&',
			},
			controllerAs: 'vm',
			controller: rgbSliderController
		});

	function rgbSliderController(){
		var vm = this;

		vm.$onInit = $onInit;

		function $onInit(){
			vm.floor = 0;
			if(vm.isTint){
				vm.floor = -255;
			}
		}

	}

})();