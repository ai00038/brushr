(function(){
	'use strict';

	angular
		.module('app')
		.component('colourButton', {
			templateUrl: 'src/common/components/colour-button/colour-button.component.html',
			bindings: {
				rgbColour: '<',
				rgbTint: '<',
				btnSize: '@'
			},
			controllerAs: 'vm',
			controller: colourButtonController
		});

	colourButtonController.$inject = ['colourUtilsService'];

	function colourButtonController(colourUtilsService){
		var vm = this;

		vm.$onInit = $onInit;
		vm.$onChanges = $onChanges;

		function $onInit(){
			buildColour();
		}

		function $onChanges(obj){
			buildColour();
		}

		function buildColour(rgbColour, rgbTint) {
			var resolvedColour = colourUtilsService.mixColourWithTint(vm.rgbColour, vm.rgbTint);
			vm.resolvedStyle = `rgb(${resolvedColour[0]}, ${resolvedColour[1]}, ${resolvedColour[2]})`;
		}

	}

})();