(function(){
	'use strict';

	angular
		.module('app')
		.component('root', {
			templateUrl: 'src/root/root.component.html',
			$routeConfig: [
				{
					path: '/main',
					component: 'main',
					name: 'Main'
				},
				{
					path: '/**',
					redirectTo: ['Main']
				}
			],
			controllerAs: 'vm',
			controller: function(){
				var vm = this;
			}
		});

})();