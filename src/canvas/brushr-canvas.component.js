(function(){
	'use strict';

	var app = angular.module('app'),
		brushrCanvas = {
			templateUrl: 'src/canvas/brushr-canvas.component.html',
			controllerAs: 'vm',
			controller: controller
		};

	function controller(){
		var vm = this;
	}	
	
	app.component('brushrCanvas', brushrCanvas);

})();