(function(){
	'use strict';

	angular
		.module('app')
		.component('currentPallette', {
			templateUrl: 'src/current-pallette/current-pallette.component.html',
			controllerAs: 'vm',
			controller: currentPalletteController
		});

	currentPalletteController.$inject = ['palletteService', 'colourUtilsService'];

	function currentPalletteController(palletteService, colourUtilsService){
		var vm = this;

		vm.$onInit = $onInit;
		vm.selectColour = selectColour;
		vm.onRgbChange = onRgbChange;
		vm.addColour = addColour;

		function $onInit(){
			vm.loading = true;

			loadData()
				.then(setDefaultColour)
				.finally(stopLoading);
		}

		function addColour(){
			console.log('adding colour');
			palletteService
				.addColourToPallette()
				.then(function(response){
					console.log('then', response);
				})
				.catch(function(err){
					console.log('catch', err);
				})
				.finally(loadData);
		}

		function loadData(){
			return palletteService
						.getPallette()
						.then(getData);
		}

		function getData(response){
			vm.currentPallette = response.data;
		}

		function setDefaultColour(){
			selectColour(vm.currentPallette[0].baseColour);
		}

		function selectColour(colour, tint){
			vm.currentColour = colourUtilsService.mixColourWithTint(colour, tint);
		}

		function stopLoading(){
			vm.loading = false;
		}

		function onRgbChange() {
			selectColour(vm.currentColour);
		}

	}

})();