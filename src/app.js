(function (){
	'use strict';

	angular
		.module('app', ['ui.bootstrap', 'ngComponentRouter', 'ngAnimate', 'rzModule']);

	angular
		.module('app')
		.value('$routerRootComponent', 'root');

	angular
		.module('app')
		.constant('apiUrl', 'http://127.0.0.1:8080/api/pallette')

})();