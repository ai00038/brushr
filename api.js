(function(){

	var express = require('express');
	var api = express();
	var fs = require("fs");
	var bodyParser = require('body-parser');
	
	api.use(bodyParser.json()); // support json encoded bodies
	api.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

	function resAllow(res) {
		res.header("Access-Control-Allow-Origin", "*");
  		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	}

	api.get('/api/pallette', function (req, res) {
		resAllow(res);
		fs.readFile( __dirname + "/resources/pallette.json", 'utf8', function (err, data) {
		   	res.end(data);
		});
	});

	api.post('/api/pallette', function(req, res){
		resAllow(res);
		console.log(req.body);
	});

	var server = api.listen(8080, function () {

		var host = server.address().address;
		var port = server.address().port;

		console.log(`API online`);

	});

})();

